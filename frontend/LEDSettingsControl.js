import React from 'react'
import './App.css'
import { Row, InputGroup, InputGroupAddon, InputGroupText, Input, Button, Col, Dropdown, DropdownItem, DropdownToggle, DropdownMenu } from 'reactstrap'
import { updateFanColor } from './socketInterface'
import ColorSelectPopOver from './ColorSelectPopOver'

const standardTwelveColorList = [
    255, 0, 0, 
    255, 0, 255, 
    255, 0, 127, 
    255, 0, 0, 
    255, 127, 0, 
    255, 255, 0, 
    127, 255, 0, 
    0, 255, 0, 
    0, 255, 127, 
    0, 255, 255,
    0, 127, 255,
    0, 0, 255
]

class LEDSettingsControl extends React.Component {

    constructor(props) {
      super(props)      
      this.state = { controllerIndex: props.controllerIndex, port: props.port, ledData: props.fanData.ledData, otherFanNames: [], copyToDropdownToggle: false }
      this.toggle = this.toggle.bind(this)
      this.updateSpeed = this.updateSpeed.bind(this)
      this.updateMode = this.updateMode.bind(this)
      this.updateNames = this.updateNames.bind(this)
      this.updateLED = this.updateLED.bind(this)  
      this.sendCopyFanData = this.sendCopyFanData.bind(this)  
      this.receiveCopyFanData = this.receiveCopyFanData.bind(this)  
      this.copyToSiblingFan = props.copyToSiblingFan.bind(this)

    }

    sendCopyFanData(event) {
        const otherController = parseInt(event.target.value.split(',')[0])
        const otherPort = parseInt(event.target.value.split(',')[1])        
        this.copyToSiblingFan(otherController, otherPort, this.state.ledData)
    }

    receiveCopyFanData(newData) {

        this.setState({ ledData: newData })

        let { mode, speed, colors } = newData
        updateFanColor({ controllerIndex: this.state.controllerIndex, port: this.state.port, colors, mode, speed })
    }

    format(number) {
        const str = number.toString(16)
        return str.length == 1 ? "0" + str : str
    }

    updateNames(data) {        
        this.setState({ name: data.myName, otherFanNames: data.otherFanNames })
    }

    updateLED(newColor, ledIndex) {
        let { mode, speed, colors } = this.state.ledData
        const startIndex = ledIndex * 3
        colors = [ ...colors.slice(0, startIndex),  ...newColor,   ...colors.slice(startIndex + 3)]        

        this.setState({ ledData: { ...this.state.ledData, colors } })        
        updateFanColor({ controllerIndex: this.state.controllerIndex, port: this.state.port, colors, mode, speed })
    }

    updateMode(event) {    
        
        let { colors, mode, speed } = this.state.ledData
        mode = Math.max(Math.min(parseInt(event.target.value), 7), 0) 

        if (mode >= 6) speed = 4 // Set to N/A
        else if (speed == 4) speed = 2 /// Set to Normal

        if (mode <= 1) colors = []  /// don't require a color
        else if (mode == 2 || mode == 7) { /// these modes require 1 color
            if (colors.length != 3) colors = [ 255,255,255 ]  /// only change if necessary
        }
        else if (mode == 6) { /// per led - 9 color
            colors = colors.length == 36 ? colors.slice(0, 27) : standardTwelveColorList.slice(9)
        }
        else {   /// standard 12 color
            if (colors.length != 36) colors = standardTwelveColorList /// only change if necessary
        }

        updateFanColor({ controllerIndex: this.state.controllerIndex, port: this.state.port, colors, mode, speed })
        this.setState({ ledData: { colors, speed, mode } })
    }

    updateSpeed(event) {
        let { colors, mode, speed } = this.state.ledData
        speed = Math.max(Math.min(parseInt(event.target.value), 4), 0)

        updateFanColor({ controllerIndex: this.state.controllerIndex, port: this.state.port, colors, mode, speed })
        this.setState({ ledData: { colors, speed, mode } })
    }

    convertToCssColorString(grbArray) {   
        if (grbArray.length == 0) return "DarkGray"     
        return `#${ this.format(grbArray[1]) }${ this.format(grbArray[0]) }${ this.format(grbArray[2]) }`
    }

    createCssFromData(data, additional) {
        return { 
            "borderColor": this.convertToCssColorString(data.ledData.colors),
            ...additional
        }
    }

    toggle(event) {
        this.setState({ copyToDropdownToggle: !this.state.copyToDropdownToggle })
    }

    render() {     
                

        let { ledData, controllerIndex, port, name  } = this.state

        if (name == undefined || name == "") name = `Controller #${controllerIndex} Port #${port}`
        
        const numberOfColorButtons = ledData.colors.length / 3      
                    
        return([

            [...Array(numberOfColorButtons)].map((ignore, index) => {
                const startIndex = index * 3
                const singleLedColor = ledData.colors.slice(startIndex, startIndex + 3)
                return(
                    <ColorSelectPopOver controllerIndex={controllerIndex} port={port} color={ singleLedColor } ledIndex={index} updateParentLed={ this.updateLED }></ColorSelectPopOver>
                )
            }),
            <h6> Fan Name: { name } </h6>,
            <Row style={{  "marginBottom": 30, "marginLeft": 10, "justifyContent": "center" }}> 
                <Col xs="3">
                        <Dropdown isOpen={ this.state.copyToDropdownToggle } toggle={ this.toggle }>
                            <DropdownToggle color="primary" caret> Copy To Fan </DropdownToggle>
                            <DropdownMenu> 
                                {
                                    this.state.otherFanNames.map(other => {
                                        if (other.name == undefined || other.name == "") other.name = `Controller #${other.controllerIndex} Port #${other.port}`
                                        return(<DropdownItem onClick={ this.sendCopyFanData } value={ `${other.controllerIndex},${other.port}` }> { other.name } </DropdownItem>)
                                    })

                                }
                            </DropdownMenu>
                        </Dropdown>
                </Col>
                <Col>
                    <InputGroup style={{"justifyContent": "center"}}>
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText className="dark-border">Mode:</InputGroupText>
                        </InputGroupAddon>
                        <Input className="dark-border custom-select" style={{ "maxWidth": 120 }} type="select" name="mode" value={ ledData.mode } onChange={ this.updateMode }>
                            <option value="0">Flow</option>
                            <option value="1">Spectrum</option>
                            <option value="2">Ripple</option>
                            <option value="3">Blink</option>
                            <option value="4">Pulse</option>
                            <option value="5">Wave</option>
                            <option value="6">Per LED</option>
                            <option value="7">Full</option>
                        </Input>
                        <Input className="dark-border" style={{ "maxWidth": 80, "backgroundColor": "#e9ecef" }} type="text" value={ "Speed:" } readOnly />
                        <Input className="dark-border custom-select" style={{ "maxWidth": 110 }} type="select" name="speed" value={ ledData.speed } onChange={ this.updateSpeed }>
                            {
                                ledData.speed == 4 
                                ?
                                <option value="4">N/A</option>
                                :
                                [<option value="3">Slow</option>,
                                <option value="2">Normal</option>,
                                <option value="1">Fast</option>,
                                <option value="0">Extreme</option>]
                    
                            }
                        </Input>
                        <InputGroupAddon addonType="append">
                            <InputGroupText className="dark-border" >Colors: </InputGroupText>
                            {
                                [...Array(numberOfColorButtons)].map((ignore, index) => {
                                    const startIndex = index * 3
                                    const singleLedColor = ledData.colors.slice(startIndex, startIndex + 3)                                
                                    return(<Button className="dark-border" style={{ "backgroundColor": this.convertToCssColorString(singleLedColor) }} id={`popC${controllerIndex}P${port}L${index}`} type="button"></Button>)
                                })
                            }
                        </InputGroupAddon>
                    </InputGroup>
                </Col>
            </Row>
        ])
    }
}
  
export default LEDSettingsControl
  