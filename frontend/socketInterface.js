import io from 'socket.io-client'
const socket = io('http://localhost:3000')

function subscribeToComputerData(callback) {
  socket.on('computerData', data => callback(data))
}

function subscribeToProfileData(callback) {
  socket.on('profileData', data => callback(data))
}

function updateFanSpeeds(data) { socket.emit('updateFanSpeeds', data) } 

function updateFanColor(data) { socket.emit('updateFanColor', data) }

function updateTargetTemp(temp) { socket.emit('updateTargetTemp', temp) } 

function sendFanName(data) { socket.emit('updateFanName', data) }

function saveFullProfile(data) { socket.emit('saveFullProfile', data) }

function activateProfile(profileName) { socket.emit('activateProfile', profileName) }

function syncLeds(data) { socket.emit('syncLeds', data) }

export { subscribeToComputerData, subscribeToProfileData, updateFanSpeeds, updateTargetTemp, updateFanColor, sendFanName, saveFullProfile, activateProfile, syncLeds }