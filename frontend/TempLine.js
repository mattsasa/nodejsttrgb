import React from 'react'
import { subscribeToComputerData } from './socketInterface'

class TempLine extends React.Component {

    constructor(props) {
      super(props)
      this.state = { cpuTemp: "" }
      subscribeToComputerData(data => {
        this.setState({ cpuTemp: data.tempData.cpuTemp })
      })
    }
  
    render() {  
        return(
          <h4> Actual CPU Die Temperature: { parseInt(this.state.cpuTemp * 10) / 10 }°C</h4>
        )
    }
  }
  
  export default TempLine
