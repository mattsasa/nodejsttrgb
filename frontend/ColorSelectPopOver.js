import React from 'react'
import { Card, Col, CardBody, CardTitle, Row, InputGroup, InputGroupAddon, InputGroupText, Input, UncontrolledPopover } from 'reactstrap'

const colorTable = {
    RED: [ 0, 255, 0 ],
    GREEN: [ 255, 0, 0 ],
    BLUE: [ 0, 0, 255 ],
    OFF: [ 0, 0, 0 ],
    WHITE: [ 255, 255, 255 ],
    PURPLE: [ 0, 255, 255 ],
    LIME: [ 255, 255, 0 ],
    YELLOW: [ 100, 255, 0],
    ORANGE: [ 30, 255, 0],
    PINK: [ 0, 255, 50],
    CYAN: [ 255, 0, 255 ]
}

class ColorSelectPopOver extends React.Component {

    constructor(props) {
      super(props)      
      this.state = { controllerIndex: props.controllerIndex, port: props.port, color: props.color, ledIndex: props.ledIndex, popoverOpen: false }
      this.updateRGB = this.updateRGB.bind(this)
      this.updateColor = this.updateColor.bind(this)
      this.toggleColorPopOver = this.toggleColorPopOver.bind(this)
    }

    updateRGB(event, rgbIndex) {     
        let { color, ledIndex } = this.state
        color[rgbIndex] = Math.max(Math.min(parseInt(event.target.value), 255), 0) 

        this.setState({ color })
        this.props.updateParentLed(color, ledIndex)
    }

    updateColor(event) {
        
        let { color, ledIndex } = this.state
        color = colorTable[event.target.value]
        
        this.setState({ color })
        this.props.updateParentLed(color, ledIndex)
    }

    toggleColorPopOver(event) {        
        this.setState({ popoverOpen: !this.state.popoverOpen })
    }

    render() {

        const { popoverOpen, controllerIndex, port, ledIndex, color } = this.state

        return (
            <UncontrolledPopover trigger="legacy" placement="top" isOpen={ popoverOpen } target={`popC${controllerIndex}P${port}L${ledIndex}`} toggle={ this.toggleColorPopOver }>
                <Card style={{ "width": 500 }}>
                <CardBody>
                <CardTitle><h5 style={{ "textAlign": "center" }}>Select Color</h5> </CardTitle>
                <Row>
                    <Col style={{ "maxWidth": 180 }}>
                        <Input className="dark-border custom-select" type="select" defaultValue="" onChange={ this.updateColor }> 
                            <option value="" hidden>Select A Color</option>
                            <option value="RED">Red</option>
                            <option value="GREEN">Green</option>
                            <option value="PINK">Pink</option>
                            <option value="YELLOW">Yellow</option>
                            <option value="BLUE">Blue</option>
                            <option value="PURPLE">Purple</option>
                            <option value="ORANGE">Orange</option>
                            <option value="LIME">Lime</option>
                            <option value="CYAN">Cyan</option>
                            <option value="OFF">Off</option>
                        </Input>
                    </Col>
                    <Col>
                        <InputGroup style={{"justifyContent": "center"}}>
                            <InputGroupAddon addonType="prepend">
                                <InputGroupText className="dark-border">RGB:</InputGroupText>
                            </InputGroupAddon>
                            <Input className="dark-border" style={ { "maxWidth": 70 } } min={0} max={255} type="number"  value={ color[1] } onChange={ (e) => this.updateRGB(e, 1) }/>
                            <Input className="dark-border" style={ { "maxWidth": 70 } } min={0} max={255} type="number"  value={ color[0] } onChange={ (e) => this.updateRGB(e, 0) }/>
                            <Input className="dark-border" style={ { "maxWidth": 70 } } min={0} max={255} type="number"  value={ color[2] } onChange={ (e) => this.updateRGB(e, 2) }/>   
                        </InputGroup>
                    </Col>
                </Row>
                </CardBody>
                </Card>
            </UncontrolledPopover>
        )
    }
}
export default ColorSelectPopOver