import React from 'react'
import './App.css'
import { Card, CardBody, CardTitle, Row, Col, Form, InputGroup, InputGroupAddon, Button, Input, Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap'
import { subscribeToComputerData, subscribeToProfileData, saveFullProfile, activateProfile, syncLeds } from './socketInterface'
import LEDSettingsControl from './LEDSettingsControl'
import { toast } from 'react-toastify'

class FanLEDControlList extends React.Component {

    constructor(props) {
      super(props)
      this.state = { fanData: [], profileDropdownToggle: false, newProfileName: "", profiles: [] }  
      this.toggle = this.toggle.bind(this)
      this.saveProfile = this.saveProfile.bind(this)
      this.syncAll = this.syncAll.bind(this)
      this.selectProfile = this.selectProfile.bind(this)
      this.handleChangeNewProfileName = this.handleChangeNewProfileName.bind(this)
      this.copyToSiblingFan = this.copyToSiblingFan.bind(this)

      subscribeToProfileData(data => {        
        this.setState({ profiles: data })
      })

      subscribeToComputerData((data) => {    

        if (data.refreshLedData) { this.setState({ fanData: [] }) }
            
        if (this.state.fanData.length != data.fanData.length) {
            this.refs = data.fanData.map(fanGroup => {
                const group = {}
                Object.keys(fanGroup).forEach(key => { group[key] = React.createRef() })
                return group
            })
            this.setState({ fanData: data.fanData })        
        }

        let namesUpdated = false
        this.refs.forEach((group, controllerIndex) => {
            Object.entries(group).forEach(([port, reference]) => {
                if (reference.current && reference.current.state.name != data.fanData[controllerIndex][port].name) {
                    namesUpdated = true
                }
            })
        })

        if (namesUpdated) {
            const fanNames = []
            this.state.fanData.forEach((controller, controllerIndex) => {
                Object.entries(controller).forEach(([port, data]) => {
                    fanNames.push({ controllerIndex, port, name: data.name })
                })
            })

            this.refs.forEach((group, controllerIndex) => {
                Object.entries(group).forEach(([port, reference]) => {
                    if (reference.current) {
                        const otherFanNames = fanNames.filter(data => (data.controllerIndex != controllerIndex || data.port != port))
                        reference.current.updateNames({ myName: data.fanData[controllerIndex][port].name, otherFanNames })
                    }
                })
            })
        }


      })

    }

    copyToSiblingFan(controllerIndex, port, newData) {
        if (this.refs[controllerIndex] && this.refs[controllerIndex][port] && this.refs[controllerIndex][port].current) {
            this.refs[controllerIndex][port].current.receiveCopyFanData(newData)
        }
    }

    toggle(event) {
        this.setState({ profileDropdownToggle: !this.state.profileDropdownToggle })
    }

    handleChangeNewProfileName(event) {
        this.setState({ newProfileName: event.target.value })
    }

    saveProfile(event) {
        event.preventDefault()
        const allFanData = this.refs.map(controller => {
            const group = {}
            Object.entries(controller).forEach(([key, value]) => {
                group[key] = value.current.state.ledData
            })
            return group
        })
        saveFullProfile({ profileName: this.state.newProfileName, allFanData })
        toast.success('Saved Profile as: ' + this.state.newProfileName)
    }

    syncAll() {
        const allFanData = this.refs.map(controller => {
            const group = {}
            Object.entries(controller).forEach(([key, value]) => {
                group[key] = value.current.state.ledData
            })
            return group
        })
        toast.success('ReSyncing all Fan Leds')
        syncLeds(allFanData)
    }

    selectProfile(event) {
        toast.success('Loading Profile: ' + event.target.innerText)
        activateProfile(event.target.innerText)
    }

    render() {           
                    
        return(
            <Card>
                <CardBody>
                <CardTitle> <h3>LED Control</h3> </CardTitle>
                <Row>
                    <Col>
                        <Form onSubmit={ this.saveProfile }>
                        <InputGroup style={{ "width": 250 }}>
                            <Input placeholder="New Profile Name" onChange={ this.handleChangeNewProfileName } />
                            <InputGroupAddon addonType="append">
                                <Button color="primary" onClick={ this.saveProfile }>Save</Button>
                            </InputGroupAddon>
                        </InputGroup>
                        </Form>
                    </Col>
                    <Col>
                        <Button color="primary" onClick={ this.syncAll }>Sync All</Button>
                    </Col>
                    <Col>
                        <Dropdown isOpen={ this.state.profileDropdownToggle } toggle={ this.toggle }>
                            <DropdownToggle color="primary" caret> Load Profile </DropdownToggle>
                            <DropdownMenu> 
                                {
                                    this.state.profiles.map(profileName => {
                                        return (<DropdownItem onClick={ this.selectProfile }>{ profileName }</DropdownItem>)
                                    })
                                }
                            </DropdownMenu>
                        </Dropdown>
                    </Col>
                </Row>
                <br/>
                { this.state.fanData.map((controller, controllerIndex) => {  
                    return Object.entries(controller).map(([port, data]) => {
                                const reference = this.refs[controllerIndex][port] 
                                return (
                                    <LEDSettingsControl ref={ reference } port={ port } fanData={ data } controllerIndex={ controllerIndex } copyToSiblingFan={ this.copyToSiblingFan } />
                                )
                            })     
                }) } 
                </CardBody>
            </Card>
        )
    }
  }
  
  export default FanLEDControlList
  