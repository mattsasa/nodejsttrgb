import React from 'react'
import { Container } from 'reactstrap'
import './App.css'
import { ToastContainer } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css'
import FanSpeedCards from './FanSpeedCards'
import TempLine from './TempLine'
import TempFanControl from './TempFanControl'
import FanLEDControlList from './FanLEDControlList'

class App extends React.Component {

  // constructor(props) { super(props) }

  render() {    
        
    return(
        <div className="App">
          <ToastContainer/>
          <header className="App-header">
            <h3> Thermaltake RGB Hub </h3>
            <TempLine/>
          </header>
          <Container>
              <FanSpeedCards/>
              <br/>
              <TempFanControl/>
              <br/>
              <FanLEDControlList/>
          </Container>
        </div>
    )
  }
}

export default App
