import React from 'react'
import { Input, InputGroupAddon, InputGroupText, InputGroup, Card, CardBody, CardTitle } from 'reactstrap'
import { subscribeToComputerData, updateTargetTemp } from './socketInterface'
import { toast } from 'react-toastify'


class TempFanControl extends React.Component {

    constructor(props) {
      super(props)
      this.update = this.update.bind(this)
      this.state = { targetTemp: '', autoFanSpeedByTemp: false }
      this.hasLoaded = false
      subscribeToComputerData(data => {
        if (this.state.autoFanSpeedByTemp && !data.tempData.autoFanSpeedByTemp) toast.info('Temperature-Based Automatic Fan Speeds Disabled')
        if (this.hasLoaded) this.setState({ autoFanSpeedByTemp: data.tempData.autoFanSpeedByTemp })
        else {
          this.setState({ autoFanSpeedByTemp: data.tempData.autoFanSpeedByTemp, targetTemp: data.tempData.targetTemp })
          this.hasLoaded = true
        }
      })
    }

    update(event) {
        this.setState({ targetTemp: event.target.value })
        updateTargetTemp( Math.max(Math.min(parseInt(event.target.value), 80), 20) )
    }
  
    render() {  
        const colorByMode = this.state.autoFanSpeedByTemp ? "border-success text-success" : "border-danger text-danger"    

        return(
          <Card>
            <CardBody>
              <CardTitle> Temperature Controlled Fan Speeds </CardTitle>
              <InputGroup  style={{"justifyContent": "center"}}>
                      <InputGroupText className={ colorByMode } >Target Temperature:</InputGroupText>
                  <Input style={{ "maxWidth": 80 }} min={20} max={80} type="number"  name="setTargetTemp" value={ this.state.targetTemp } onChange={ this.update }/>
                  <InputGroupAddon addonType="append">°C</InputGroupAddon>
              </InputGroup>
            </CardBody>
          </Card>
        )
    }
  }
  
  export default TempFanControl