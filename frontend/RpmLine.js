import React from 'react'
import { InputGroupText } from 'reactstrap'

class RpmLine extends React.Component {

    constructor(props) {
      super(props)
      this.state = { speedPercent: "", RPMs: "" }
    }

    updateData(data) { this.setState(data) }
  
    render() {  
        return(
          <InputGroupText >  RPMs: { this.state.rpms }  </InputGroupText>
        )
    }
  }
  
  export default RpmLine
