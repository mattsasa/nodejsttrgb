import React from 'react'
import { Input, InputGroupAddon, InputGroupText, InputGroup, Card, CardDeck, Col, Form, CardBody, CardTitle, CardSubtitle } from 'reactstrap'
import { subscribeToComputerData, updateFanSpeeds, sendFanName } from './socketInterface'
import RpmLine from "./RpmLine"

class FanSpeedCards extends React.Component {

    constructor(props) {
      super(props)      
      this.state = { fanData: [] }
      this.updateSpeed = this.updateSpeed.bind(this)
      this.updateFanName = this.updateFanName.bind(this)
      subscribeToComputerData((data) => {
        if (this.state.fanData.length != data.fanData.length) {
            this.speedLineRefs = data.fanData.map(fanGroup => {
                const group = {}
                Object.keys(fanGroup).forEach(key => { group[key] = React.createRef() })
                return group
            })
            this.setState({ fanData: data.fanData })
        }

        ///// check for name updates
        if (this.state.fanData.length != 0) { 
            let anyUpdateNecessary = false        
            data.fanData.forEach((controller,controllerIndex) => {
                Object.keys(controller).forEach((port) => {                    
                    if(this.state.fanData[controllerIndex][port].name != data.fanData[controllerIndex][port].name) anyUpdateNecessary = true
                })
            })
            if (anyUpdateNecessary) this.setState({ fanData: data.fanData })
        }
        
        if (data.tempData.autoFanSpeedByTemp) { this.setState({ fanData: data.fanData }) }

        this.speedLineRefs.forEach((group, controllerIndex) => {
            Object.entries(group).forEach(([port, reference]) => {
                if (reference.current) reference.current.updateData(data.fanData[controllerIndex][port].speedData)
            })
        })

      })
    }

    updateFanName(event, controllerIndex, port) {
        sendFanName({ key: `c${controllerIndex}p${port}`, name: event.target.value })
        this.state.fanData[controllerIndex][port].name = event.target.value
        this.setState({ fanData: this.state.fanData }) 
    }

    updateSpeed(event) {      
        
        const controllerIndex = event.target.parentElement.parentElement.parentElement.parentElement.id
        const port = event.target.id

        const { fanData } = this.state
        
        if (parseInt(event.target.value))
            fanData[controllerIndex][port].speedData.speedPercent = parseInt(event.target.value)

        this.setState(fanData)

        updateFanSpeeds({ controllerIndex, port, speed: Math.max(Math.min(parseInt(event.target.value), 100), 20) })
        
    }

    render() {     
                    
      return(
        <CardDeck style={{"justifyContent": "center"}}>
            { this.state.fanData.map((controller, controllerIndex) => {                 
                return (
                    <Card key={controllerIndex} style={{ "maxWidth": 540, "minWidth": 540 }}>
                        <CardBody id={controllerIndex}>
                            <CardTitle>Thermaltake Controller #{controllerIndex}</CardTitle>
                            <CardSubtitle>{ Object.values(controller).length } fans connected to this controller</CardSubtitle>
                            
                            { 
                                Object.entries(controller).map(([port, data]) => { return ( 
                                    
                                    <Form inline key={port} onSubmit={ e => { e.preventDefault() }}>
                                        <Col style={{ "maxWidth": 170, "paddingRight": 0, "paddingLeft": 0 }}>
                                            <Input style={{ "maxWidth": 150, "backgroundColor": "#e9ecef" }} type="text" placeholder={`Fan Port #${ port }`} value={ data.name } onChange={ e => { this.updateFanName(e, controllerIndex, port) }}/>
                                        </Col>
                                        <Col style={{ "paddingLeft": 0, "paddingRight": 0 }}>
                                            <InputGroup>
                                                <InputGroupAddon addonType="prepend">
                                                    <InputGroupText>Speed:</InputGroupText>
                                                </InputGroupAddon>
                                                <Input style={{ "maxWidth": 80 }} min={20} max={100} type="number"  name="setSpeedData" id={port} value={ data.speedData.speedPercent } onChange={ this.updateSpeed }/>
                                                <InputGroupAddon addonType="append">%</InputGroupAddon>
                                            </InputGroup>
                                        </Col>
                                        <Col style={{ "maxWidth": 130, "paddingLeft": 0 }}>
                                            <RpmLine ref={ this.speedLineRefs[controllerIndex][port] } />
                                        </Col>
                                    </Form>
                                ) })
                            }
                        </CardBody>
                    </Card>
                )
            }) }
        </CardDeck>
      )
    }
  }
  
  export default FanSpeedCards
  