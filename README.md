# nodejsTTRGB

nodejs server for controlling thermaltake rgb fan controllers

![Screenshot_from_2020-04-30_21-05-57](/uploads/44ff800255825ea09c46270c1458f93b/Screenshot_from_2020-04-30_21-05-57.png)

![Screenshot_from_2020-04-30_21-05-57](/uploads/d2cd4a514c19cf013889e0eabcd6aaef/LedControl00.png)


# Binary Releases
## v0.1.0-b1
Linux: [nodejsTTRGB-linux.zip](/uploads/b90ed9458b62f96f5a68a3884d827fb4/nodejsTTRGB-linux.zip)

Windows: [nodejsttrgb-win.zip](/uploads/beaca144caef679775fad8b880824c39/nodejsttrgb-win.zip)

## Instructions
*  Download
*  Extract
*  Run the executable
*  Access at localhost:3000

### Additional Steps for Windows 10
*  Follow these [steps](https://jbcomp.com/disable-device-driver-signing-in-windows-10/) to boot windows without driver signing enforced
*  Install WinUsb driver using [Zadig](https://zadig.akeo.ie/) program - Select devices with VendorId: 264A (Thermaltake), and install 'WinUSB'