const usb = require('usb')
const si = require('systeminformation')
const colors = require('colors')
const fs = require('fs')
const express = require('express')
const path = require('path')
const app = express()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const port = 3000

const isWin = process.platform === "win32"

let userConfig
const updateDataRate = 500
let targetTemp = 40
let autoFanSpeedByTemp = false

const PROTOCOL_GET = 0x33
const PROTOCOL_SET = 0x32

const PROTOCOL_FAN = 0x51
const PROTOCOL_LIGHT = 0x52

const LIGHT_MODES = {
    FLOW: 0x00,
    SPECTRUM: 0x04,
    RIPPLE: 0x08,
    BLINK: 0x0c,
    PULSE: 0x10,
    WAVE: 0x14,
    BY_LED: 0x18,
    FULL: 0x19
}

const colorLengthByMode = {
    0: 0,
    1: 0,
    2: 1,
    3: 12,
    4: 12,
    5: 12,
    6: 9,
    7: 1
}

const COLORS = {
    RED: [ 0, 255, 0 ],
    GREEN: [ 255, 0, 0 ],
    BLUE: [ 0, 0, 255 ],
    OFF: [ 0, 0, 0 ],
    WHITE: [ 255, 255, 255 ],
    PURPLE: [ 0, 255, 255 ],
    LIME: [ 255, 255, 0 ],
    YELLOW: [ 100, 255, 0],
    ORANGE: [ 30, 255, 0],
    PINK: [ 0, 255, 50],
    CYAN: [ 255, 0, 255 ]
}

const colorMap = [
    COLORS.GREEN,
    COLORS.ORANGE,
    COLORS.CYAN,
    COLORS.RED,
    COLORS.WHITE,
    COLORS.PURPLE,
    COLORS.YELLOW,
    COLORS.PINK,
    COLORS.LIME,
    COLORS.BLUE
]

function saveConfig() {
    fs.writeFile('userConfig.json', JSON.stringify(userConfig), (err) => {
        if (err) console.log(`${err}`.red)
    })
}

function loadConfig() {
    fs.readFile('userConfig.json', (err, data) => {
        if (err) {
            console.log(`No Configuration file found, creating a fresh one`.yellow)
            userConfig = {
                fanNames: {},
                fullProfiles: {}
            }
            saveConfig()
        } else {
            userConfig = JSON.parse(data)
            console.log(`Successfully Loaded Config`.green)
        }
      })
}

const controllerList = []
let cpuTemp
let last_speed = 50
let fanTempInterval
let refreshLedData = false

function resetAsync(device) {
    return new Promise((resolve, reject) => {
        device.reset(error => {
            if (error) reject(error)
            else resolve()
        })
    })
}

function sendDataAsync(index, data) {
    return new Promise((resolve, reject) => {
        controllerList[index].outputHandle.transfer(data, error => {
            if (error) reject(error)
            else resolve()
        })
    })
}

function sendDataByIndex(index, data) {    
    controllerList[index].outputHandle.transfer(data, error => {
        if (error) console.log("there was an error sending the data: " + error);
    })
}

function sendData(handle, data) {    
    handle.transfer(data, error => {
        if (error) console.log("there was an error sending the data: " + error);
    })
}

function sendDataToAllFans(data, protocol) {    

    controllerList.forEach((controller) => {
        Object.keys(controller.connectedFans).forEach(port => {
            sendData(controller.outputHandle, [PROTOCOL_SET, protocol, port, ...data])
        })
    })

}

function activateProfile(profileName) {
    console.log(`Activating User Profile: ${ profileName }`.cyan)
    const profile = userConfig.fullProfiles[profileName]
    profile.forEach((controller, controllerIndex) => {
        Object.entries(controller).forEach(([port, data]) => {
            let mode
            if (data.mode == 7) mode = 0x19
            else if (data.mode == 6) mode = 0x18
            else mode = (data.mode * 4) + data.speed    
            sendDataByIndex(controllerIndex, [PROTOCOL_SET, PROTOCOL_LIGHT, port, mode, ...data.colors])
        })
    })
    refreshLedData = true
}

function syncLeds(data) {
    data.forEach((controller, controllerIndex) => {
        Object.entries(controller).forEach(([port, data]) => {
            let mode
            if (data.mode == 7) mode = 0x19
            else if (data.mode == 6) mode = 0x18
            else mode = (data.mode * 4) + data.speed    
            sendDataByIndex(controllerIndex, [PROTOCOL_SET, PROTOCOL_LIGHT, port, mode, ...data.colors])
        })
    })
}

function readSingleMessage(handle) {
    return new Promise((resolve, reject) => {
        handle.removeAllListeners()
        handle.on('data', buffer => resolve(buffer))
    })
}


async function sendCommandAndReadResponse(controller, commandData) {
    sendData(controller.outputHandle, commandData)
    const expectedProtocol = commandData[1]
    let responseMsg = await readSingleMessage(controller.inputHandle)
    while (responseMsg[1] != expectedProtocol || responseMsg[0] != PROTOCOL_GET) responseMsg = await readSingleMessage(controller.inputHandle)
    return [...responseMsg]
}

function turnOnTempFanSpeeds() {
    fanTempInterval = setInterval(() => {
        const multiplier = 5
        speed = (((cpuTemp - targetTemp) * multiplier) + last_speed) / 2
        speed = Math.max(Math.min(parseInt(speed), 100), 20)
        sendDataToAllFans([0x01, speed], PROTOCOL_FAN)
        last_speed = speed
    }, 1000)    
}

async function getFanData() {

    return await Promise.all( controllerList.map(async (controller, controllerIndex) => {
        let object = {}
        for (let p = 1; p <= 5; p++) {
            const fanSpeedResult = await sendCommandAndReadResponse(controller, [PROTOCOL_GET, PROTOCOL_FAN, p]) 
            const fanLEDResult = await sendCommandAndReadResponse(controller, [PROTOCOL_GET, PROTOCOL_LIGHT, p])
            if (fanSpeedResult[5] == 0 && fanSpeedResult[6] == 0) { continue }
            let mode, speed
            if (fanLEDResult[3] >= 0x18) {
                mode = fanLEDResult[3] == 0x18 ? 6 : 7
                speed = 4
            } else {
                mode = Math.floor( fanLEDResult[3] / 4 )
                speed = fanLEDResult[3] % 4
            }
            const colorLength = colorLengthByMode[mode] * 3
            const colors = fanLEDResult.slice(4, 4 + colorLength)

            object[p] = { 
                name: userConfig.fanNames[`c${controllerIndex}p${p}`],
                speedData: { speedPercent: fanSpeedResult[4], rpms: (fanSpeedResult[6]*256 + fanSpeedResult[5]) },
                ledData: { mode, speed, colors }
            }
        }        
        return object
    }))
}

function getTempData() {
    return { cpuTemp, targetTemp, autoFanSpeedByTemp }
}

async function getAllComputerData() {
    return { tempData: getTempData(), fanData: await getFanData(), refreshLedData }
}


async function setupEndPoints() {

    usb.getDeviceList().forEach(device => {
        if (device.deviceDescriptor.idVendor == 9802) { controllerList.push({ device }) }
    })
    
    if (controllerList.length == 0) console.log(`Error: No Thermaltake Fan Controllers Detected`.red)
    else console.log(`${controllerList.length} Thermaltake Fan Controllers Detected`.green)

    controllerList.forEach(async (controller, index) => {
        controller.device.open()
        /// seems to not be necessary?
        resetAsync(controller.device)
        controller.interface = controller.device.interface(0)
        if (!isWin && controller.interface.isKernelDriverActive()) controller.interface.detachKernelDriver()
        /// seems to not be necessary?  well probably is?
        controller.interface.claim()
        controller.outputHandle = controller.interface.endpoints.find(endpoint => endpoint.direction == 'out')
        controller.inputHandle = controller.interface.endpoints.find(endpoint => endpoint.direction == 'in')
        controller.inputHandle.startPoll(3, 64)

        /// Sends Init Command to controller
        sendData(controller.outputHandle, [0xfe, PROTOCOL_GET])

        controller.connectedFans = {}
        for (let p = 1; p <= 5; p++) {
            const fanSpeedResult = await sendCommandAndReadResponse(controller, [PROTOCOL_GET, PROTOCOL_FAN, p])
            const fanLEDResult = await sendCommandAndReadResponse(controller, [PROTOCOL_GET, PROTOCOL_LIGHT, p])
            if (fanSpeedResult[5] == 0 && fanSpeedResult[6] == 0) continue
            controller.connectedFans[p] = { 
                speedData: { speedPercent: fanSpeedResult[4], rpms: (fanSpeedResult[6]*256 + fanSpeedResult[5]) },
                ledData: { }
            }
        }
        console.log(`${Object.keys(controller.connectedFans).length} fans connected on controller ${index}`.blue);
    })
    
}

setInterval(() => {
    si.cpuTemperature().then( data => cpuTemp = data.main )
}, updateDataRate)

loadConfig()

setupEndPoints()

const isStandaloneExecutable = path.basename(process.execPath, ".exe") == 'node' || path.basename(process.execPath) == 'nodejs'

const execPath = isStandaloneExecutable  ? __dirname : path.dirname(process.execPath)
const filesToServe = path.join(execPath, 'build')

app.use(express.static(filesToServe))
server.listen(port, () => {
    console.log("Serving files at: " + filesToServe)
})

let updateClientInterval

io.on('connection', async (socket) => {
    console.log(`A browser client has connected to the GUI`.white)
    socket.emit('computerData', await getAllComputerData())
    socket.emit('profileData', Object.keys(userConfig.fullProfiles))

    clearInterval(updateClientInterval)
    updateClientInterval = setInterval(async () => {           
        socket.emit('computerData', await getAllComputerData())
        refreshLedData = false
    }, updateDataRate)
    
    socket.on('updateFanColor', data => {          
        let mode
        if (data.mode == 7) mode = 0x19
        else if (data.mode == 6) mode = 0x18
        else mode = (data.mode * 4) + data.speed    
        sendDataByIndex(data.controllerIndex, [PROTOCOL_SET, PROTOCOL_LIGHT, data.port, mode, ...data.colors])
    })

    socket.on('updateFanSpeeds', data =>  {
        clearInterval(fanTempInterval)
        autoFanSpeedByTemp = false
        sendDataByIndex(data.controllerIndex, [PROTOCOL_SET, PROTOCOL_FAN, data.port, 0x01, data.speed])
    })

    socket.on('updateTargetTemp', data => {
        targetTemp = data
        autoFanSpeedByTemp = true
        clearInterval(fanTempInterval)
        turnOnTempFanSpeeds()
    })

    socket.on('updateFanName', data => {
        userConfig.fanNames[data.key] = data.name
        saveConfig()
    })

    socket.on('saveFullProfile', data => {
        userConfig.fullProfiles[data.profileName] = data.allFanData
        saveConfig()
        console.log(`Saved Profile: ${ data.profileName }`.cyan)
        socket.emit('profileData', Object.keys(userConfig.fullProfiles))
    })

    socket.on('activateProfile', profileName => activateProfile(profileName))

    socket.on('syncLeds', data => syncLeds(data))

    socket.on('disconnect', () => clearInterval(updateClientInterval))
})

app.get("/changeColorAllFans", async (req, res) => { 

    const chosenColor = COLORS[req.query.color.toUpperCase()]

    sendDataToAllFans([LIGHT_MODES.FULL, ...chosenColor], PROTOCOL_LIGHT)

    res.send("done")
})




